const express = require('express')
const path = require('path')
const app = express();

const port = process.env.PORT || 8000

app.listen(port, () => {
    console.log("Server is running on port ", port);
    console.log(path.resolve(process.cwd(), "public", "index.html"));
})

app.get('/', (req, res) => {
    res.sendFile(path.resolve(process.cwd(), "public", "index.html"))
})   